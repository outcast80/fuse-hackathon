package com.digia.fusesender;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.spi.DataFormat;
import org.springframework.stereotype.Component;

import java.net.Inet4Address;
import java.util.Date;
import java.util.Map;

@Component
public class JokeRouteBuilder extends RouteBuilder {

    private static final String SEND_ROUTE = "direct:send-rest";

    @Override
    public void configure() throws Exception {

        DataFormat jacksonData = new JacksonDataFormat(Map.class);

        restConfiguration()
                .apiContextPath("/api-doc")
                .apiProperty("api.title", "Jokes for the World REST API")
                .apiProperty("api.version", "1.0")
                .apiProperty("cors", "true")
                .apiProperty("base.path", "camel/")
                .apiProperty("api.path", "/")
                .apiProperty("host", "")
                .apiContextRouteId("doc-api")
                .component("servlet")
                .bindingMode(RestBindingMode.json);

        rest("/joke")
                .description("Give me a joke!")
                .get("/").outType(String.class)
                .route()
                .routeId("rest-api-route")
                .process(exchange -> {
                    exchange.getIn().getHeaders().clear();
                })
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .to("https4://api.chucknorris.io/jokes/random?brigdeEndpoint=true")
                .convertBodyTo(String.class)
                .log("received ${body}")
                .unmarshal(jacksonData)
                .process(exchange -> {
                    Map map = exchange.getIn().getBody(Map.class);
                    String msg = (String)map.get("value");
                    exchange.getIn().setBody(msg);
                    exchange.getIn().getHeaders().clear();
                })
                .log("sending ${body}")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant("200"))
                .setHeader(Exchange.CONTENT_TYPE, constant("text/plain"));
    }
}
